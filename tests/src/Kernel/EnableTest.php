<?php

namespace Drupal\Tests\coupon_for_role\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the most basic functionality of the module.
 *
 * @group coupon_for_role
 */
class EnableTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'coupon_for_role',
  ];

  /**
   * Test enable.
   */
  public function testEnable() {
    self::assertTrue(TRUE);
  }

}
