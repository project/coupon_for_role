<?php

namespace Drupal\coupon_for_role\Exception;

/**
 * Exception for when a coupon has already been used.
 */
class CouponAlreadyUsedException extends \Exception {}
